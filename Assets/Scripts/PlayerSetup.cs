﻿using UnityEngine;
using UnityEngine.Networking;

public class PlayerSetup : NetworkBehaviour {
	

	[SerializeField]
	Behaviour[]ComponentsToDisable;

	[SerializeField]
	GameObject playerUIprefab;
	private GameObject playerUIInstance;

	//public PlayerSetup PS;
	//public Transform spawnPoint4;

	void Start () {
		if (!isLocalPlayer) {
			for (int i = 0; i < ComponentsToDisable.Length; i++) {
				ComponentsToDisable [i].enabled = false;
			}
		} 
		else {
			playerUIInstance = Instantiate (playerUIprefab);
			playerUIInstance.name = playerUIprefab.name;
		}

		if (isServer) {

		}
		else{
			
			}
	}
	
	// Update is called once per frame
	void Update () {
	}
}



	
