﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerUI : NetworkBehaviour {

	[SerializeField]
	GameObject pauseMenu;

	void Start () {
		
		PauseMenu.IsOn = false;
	}

	// Update is called once per frame
	void Update () {
	
		if(Input.GetKeyDown(KeyCode.Escape)){

			TogglePauseMenu ();
		}
	}

	void TogglePauseMenu(){
		
		pauseMenu.SetActive (!pauseMenu.activeSelf);
		PauseMenu.IsOn = pauseMenu.activeSelf;
	}
}
