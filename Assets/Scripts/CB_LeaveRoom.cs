﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

[RequireComponent(typeof(Collider))]
public class CB_LeaveRoom : NetworkBehaviour, ICardboardGazeResponder {

	private Vector3 startingPosition;
	private Vector3 updatedPosition;
	public int czas;
	// Use this for initialization
	void Start () {
		startingPosition = transform.localPosition;
		updatedPosition = transform.localPosition;
		SetGazedAt(false);
		czas = 0;
	}

	void LateUpdate() {
		Cardboard.SDK.UpdateState();
		if (Cardboard.SDK.BackButtonPressed) {
			Application.Quit();
		}
	}
	public void SetGazedAt(bool gazedAt) {
	}


	void Update () {

		/*if (PauseMenu.IsOn)
			return;*/

	}



	public void Reset(){
		transform.localPosition = startingPosition;
	}


	public void ToggleDistortionCorrection() {
		Cardboard.SDK.DistortionCorrection = Cardboard.DistortionCorrectionMethod.Unity;
		//break;
		//}
	}

	public void Licz_czas(){
		do{
			czas++;
			Debug.Log("Wartość czasu w trakcie liczenia: " + czas);
		}while(czas<100);
	}

	public void Zeruj_czas(){
		czas = 0;
		Debug.Log("Wartość czasu zerowanie: " + czas);
	}
	#region ICardboardGazeResponder implementation

	/// Called when the user is looking on a GameObject with this script,
	/// as long as it is set to an appropriate layer (see CardboardGaze).
	public void OnGazeEnter() {
		SetGazedAt(true);
		Licz_czas ();
		// włacza się licznik który inkrementuje do jakiegoś pułapu
	}

	/// Called when the user stops looking on the GameObject, after OnGazeEnter
	/// was already called.
	public void OnGazeExit() {
		SetGazedAt(false);
		Zeruj_czas ();
	
		//licznik się zeruje
		//Reset ();
	}

	// Called when the Cardboard trigger is used, between OnGazeEnter
	/// and OnGazeExit.
	public void OnGazeTrigger() {
		
		// sprawdzam czy licznik zainkrementował do pułapu, jeśli tak to włączyć leave room

	}

	#endregion
}

